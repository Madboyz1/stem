translate([5,5,25])
	rotate([0,0,45])
		cylinder(r1=7,r2=0,h=10,$fn=4);


difference()
{
cube([10,10,25]);
translate([-.01,5,20])
	rotate([0,90,0])
		cylinder(r=.75,h=12,$fn=64);
}